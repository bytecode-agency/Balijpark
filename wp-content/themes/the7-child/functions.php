<?php

/*
|----------------------------------------------------
|  Framework: setup of the custom functions
|----------------------------------------------------
*/

require_once("inc/cleanup.php"); // Cleanup of the admin area
require_once("inc/custom-theme.php"); // Adds custom features to the website
require_once("inc/disable-comments.php"); // Disables comments site-wide

define( 'DISALLOW_FILE_EDIT', true ); //Disable theme and plugin editors
add_filter('jetpack_development_mode', '__return_true' ); // Sets Jetpack to dev mode
add_action( 'admin_head', 'gp_remove_menus' ); // Remove menu's for Dev -> Production
add_action( 'admin_bar_menu', 'remove_some_nodes_from_admin_top_bar_menu', 999 ); // Remove customizer option from top bar
add_action( 'admin_menu', 'disable_customizer' ); // Remove customizer from wp-admin

/*************** CUSTOM PHP BELOW ***************/

add_action('wp_head', 'add_google_analytics');
function add_google_analytics() { ?>
  <script type="text/javascript" data-cfasync="false">
  	/* Function to detect opted out users */
  	function __gaTrackerIsOptedOut() {
  		return document.cookie.indexOf(disableStr + '=true') > -1;
  	}

  	/* Disable tracking if the opt-out cookie exists. */
  	var disableStr = 'ga-disable-UA-41177472-1';
  	if ( __gaTrackerIsOptedOut() ) {
  		window[disableStr] = true;
  	}

  	/* Opt-out function */
  	function __gaTrackerOptout() {
  	  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
  	  window[disableStr] = true;
  	}

  	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  	})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

  	__gaTracker('create', 'UA-41177472-1', 'auto');
  	__gaTracker('set', 'forceSSL', true);
  	__gaTracker('send','pageview');
  </script>
<?php }

function add_custom_scripts_balijpark () {
  wp_enqueue_script ( 'custom-script-balijpark', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'add_custom_scripts_balijpark');


// Redirect 'category/panden' category to 'Beschikbare panden'
add_filter('template_redirect', 'template_redirect_filter', 10, 3);
function template_redirect_filter( $url ) {
    if ( is_category( 'panden' ) ) {
        $url = site_url( '/beschikbare-panden' );
        wp_safe_redirect( $url, 301 );
        exit;
    }
    return $url;
}

// Previous -> Vorige
add_filter( 'gettext', 'replace_text2' );
function replace_text2( $text ){
    if( $text === 'Previous' ) {
        $text = 'Vorige';
    } return $text;
}

// Next -> Volgende
add_filter( 'gettext', 'replace_text1' );
function replace_text1( $text ){
    if( $text === 'Next' ) {
        $text = 'Volgende';
    } return $text;
}
